<?php


define('SC_DISEASE_FREE', 'Disease-free Survival');
define('SC_OVERALL', 'Overall Survival');

/**
* Display help and module information
* @param path which path of the site we're displaying help
* @param arg array that holds the current path as would be returned from arg() function
* @return help text for the path
*/
function survival_curve_help($path, $arg) {
  $output = '';  //declare your output variable
  switch ($path) {
    case "admin/help#survival_curve":
      $output = '<p>'.  t("Survival Curves For BMT Done Patients. ") .'</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_menu().
 */
function survival_curve_menu() {
  $items['patients/survival_curve'] = array(
  'title' => 'Survival Curves',
  'description' => 'Survival Curves for BMT Done Patients',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('survival_curve_form'),
  'access arguments' => array('access content'),
  'type' => MENU_LOCAL_TASK,
  );
  return $items;
}


function survival_curve_form() {
  $form['reports'] = array(
    '#title' => t('Survival Curve Parameters'),
    '#description' => 'Please select the Parameters for finding Survival Curve for BMT Done and BMT Undergoing Patients',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['reports']['risk_groups'] = array(
    '#title' => t('Risk Groups'),
    '#type' => 'select',
    '#options' =>sc_get_risk_group(),
    '#description' => t('Please select the Risk Group for Survival Curves'),
    '#multiple' => TRUE,
  );

  $form['reports']['treatment_regimes'] = array(
    '#title' => t('Treatment Regimes '),
    '#type' => 'select',
    '#options' =>sc_get_treatment_regimes(),
    '#description' => t('Please select the Treatment Regimes for Survival Curves'),
    '#multiple' => TRUE,
  );

  $form['reports']['centres'] = array(
    '#title' => t('Centres'),
    '#type' => 'select',
    '#options' =>sc_get_centres(),
    '#description' => t('Please select the centre for Survival Curves'),
    '#multiple' => TRUE,
  );


  $form['reports']['diseases'] = array(
    '#title' => t('Diseases '),
    '#type' => 'select',
    '#options' =>sc_get_diseases(),
    '#description' => t('Please select the Diseases for Survival Curves'),
    '#multiple' => TRUE,
  );

  $form['reports']['major_complications'] = array(
    '#title' => t('Major Complications '),
    '#type' => 'select',
    '#options' =>sc_get_major_complications(),
    '#description' => t('Please select the Major Complications for Survival Curves'),
    '#multiple' => TRUE,
  );

  $form['reports']['study_categories'] = array(
    '#title' => t('Study Categories '),
    '#type' => 'select',
    '#options' =>sc_get_study_categories(),
    '#description' => t('Please select the Study Categories for Survival Curves'),
    '#multiple' => TRUE,
  );

  $form['reports']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process'),
  );

  if(!isset($_SESSION["survival_curve"])){
    $_SESSION['survival_curve']="";
  }

  $form['final_target'] = array(
   // '#prefix' => '<div id="charts">',
    '#markup' => $_SESSION["survival_curve"],
   // '#suffix' => '</div>',
  );

  $_SESSION["survival_curve"]=NULL;
  return $form;
}

function survival_curve_getdiagnosis(){
  return drupal_map_assoc(array(SC_OVERALL,SC_DISEASE_FREE));
}

function survival_curve_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  // unset($form_state['values']['submit'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'] ,$form_state['values']['form_build_id']);
  $parameters = array();
  $parameters['centres'] = $form_state['values']['centres'];
  $parameters['study_categories'] = $form_state['values']['study_categories'];
  $parameters['major_complications'] = $form_state['values']['major_complications'];
  $parameters['diseases'] = $form_state['values']['diseases'];
  $parameters['treatment_regimes'] = $form_state['values']['treatment_regimes'];
  $parameters['risk_groups'] = $form_state['values']['risk_groups'];
  $sc_parameters = drupal_map_assoc(array(SC_OVERALL, SC_DISEASE_FREE));
  $_SESSION["survival_curve"]= survival_curve_generate($sc_parameters,$parameters);
  $form_state['rebuild'] =TRUE;
}

function survival_curve_generate($sc_parameters,$parameters){
  $survival_arr = survival_curve_data($sc_parameters,$parameters);
  return survival_curve_render($sc_parameters,$survival_arr);
}

function sc_get_centres(){
  $centres= array();
  $query=db_select('og', 'centre')
    ->fields('centre', array('gid','label'))
    ->condition('centre.state','1');
  $query->orderBy('label', 'ASC');
  $result = $query->execute();
  foreach ($result as $centre) {
    $centres[$centre->gid]= $centre->label;
  }
  return $centres;
}

function sc_get_treatment_regimes(){
  $treatment_regimes= array();
  $query=db_select('taxonomy_term_data', 'treatment_regimes')
    ->fields('treatment_regimes', array('tid','name'))
    ->condition('treatment_regimes.vid','5');
  $query->orderBy('name', 'ASC');
  $result = $query->execute();
  foreach ($result as $treatment_regime) {
    $treatment_regimes[$treatment_regime->tid]= $treatment_regime->name;
  }
  return $treatment_regimes;
}

function sc_get_risk_group(){
  $risk_groups= array();
  $query=db_select('taxonomy_term_data', 'risk_groups')
    ->fields('risk_groups', array('tid','name'))
    ->condition('risk_groups.vid','4');
  $query->orderBy('name', 'ASC');
  $result = $query->execute();
  foreach ($result as $risk_group) {
    $risk_groups[$risk_group->tid]= $risk_group->name;
  }
  return $risk_groups;
}

function sc_get_study_categories(){
  $study_categories= array();
  $query=db_select('taxonomy_term_data', 'study_categories')
    ->fields('study_categories', array('tid','name'))
    ->condition('study_categories.vid','25');
  $query->orderBy('name', 'ASC');
  $result = $query->execute();
  foreach ($result as $study_category) {
    $study_categories[$study_category->tid]= $study_category->name;
  }
  return $study_categories;
}

function sc_get_major_complications(){
  $major_complications= array();
  $query=db_select('taxonomy_term_data', 'major_complications')
    ->fields('major_complications', array('tid','name'))
    ->condition('major_complications.vid','24');
  $query->orderBy('name', 'ASC');
  $result = $query->execute();
  foreach ($result as $complication) {
    $major_complications[$complication->tid]= $complication->name;
  }
  return $major_complications;
}

function sc_get_diseases(){
  $diseases= array();
  $query=db_select('taxonomy_term_data', 'diseases')
    ->fields('diseases', array('tid','name'))
    ->condition('diseases.vid','2');
  $query->orderBy('name', 'ASC');
  $result = $query->execute();
  foreach ($result as $disease) {
    $diseases[$disease->tid]= $disease->name;
  }
  return $diseases;
}

function sc_get_survdata($parameters){
  $query=db_select('node', 'node')
    ->fields('node', array('nid'))
    ->condition('node.type','patient');
  $query->distinct();
  $query->innerJoin('field_data_field_status_descrp_internal', 'status_internal', 'status_internal.entity_id = node.nid');
  $query->Condition('status_internal.field_status_descrp_internal_tid',array('846','845'),'IN');
  $query->Condition('status_internal.entity_type','node');
  $query->Condition('status_internal.deleted','0');

  $query->innerJoin('field_data_field_patient_elapsed_time_bmt', 'patient_elapsed_bmt'
    , 'patient_elapsed_bmt.entity_id = node.nid');
  $query->Condition('patient_elapsed_bmt.entity_type','node');
  $query->Condition('patient_elapsed_bmt.deleted','0');
  $query->addField('patient_elapsed_bmt', 'field_patient_elapsed_time_bmt_value','elapsed_time_bmt_value');

  $query->innerJoin('field_data_field_patient_overall_survival', 'patient_overall_survival'
    , 'patient_overall_survival.entity_id = node.nid');
  $query->Condition('patient_overall_survival.entity_type','node');
  $query->Condition('patient_overall_survival.deleted','0');
  $query->addField('patient_overall_survival', 'field_patient_overall_survival_value','overall_survival_value');

  $query->innerJoin('field_data_field_patient_thal_free_survival', 'patient_thal_free_survival'
    , 'patient_thal_free_survival.entity_id = node.nid');
  $query->Condition('patient_thal_free_survival.entity_type','node');
  $query->Condition('patient_thal_free_survival.deleted','0');
  $query->addField('patient_thal_free_survival'
    , 'field_patient_thal_free_survival_value','thal_free_survival_value');

  //Conditional Queries bases on Parameter selected
  if(!empty($parameters['risk_groups'])){
    $query->innerJoin('field_data_field_patient_risk_factor', 'patient_risk' , 'patient_risk.entity_id = node.nid');
    $query->Condition('patient_risk.field_patient_risk_factor_tid',$parameters['risk_groups'],'IN');
    $query->Condition('patient_risk.entity_type','node');
    $query->Condition('patient_risk.deleted',0);
  }

  if(!empty($parameters['treatment_regimes'])){
    $query->innerJoin('field_data_field_patient_treatment_regime', 'patient_treatment_regimes' , 'patient_treatment_regimes.entity_id = node.nid');
    $query->Condition('patient_treatment_regimes.field_patient_treatment_regime_tid',$parameters['treatment_regimes'],'IN');
    $query->Condition('patient_treatment_regimes.entity_type','node');
    $query->Condition('patient_treatment_regimes.deleted',0);
  }

  if(!empty($parameters['centres'])){
    $query->innerJoin('og_membership', 'og_membership' , 'og_membership.etid = node.nid');
    $query->Condition('og_membership.entity_type','node');
    $query->Condition('og_membership.gid',$parameters['centres'],'IN');
  }

  if(!empty($parameters['diseases'])){
    $query->innerJoin('field_data_field_patient_disease', 'patient_disease' , 'patient_disease.entity_id = node.nid');
    $query->Condition('patient_disease.field_patient_disease_tid',$parameters['diseases'],'IN');
    $query->Condition('patient_disease.entity_type','node');
    $query->Condition('patient_disease.deleted',0);
  }

  if(!empty($parameters['major_complications'])){
    $query->innerJoin('field_data_field_patient_major_complication', 'patient_complication' , 'patient_complication.entity_id = node.nid');
    $query->Condition('patient_complication.field_patient_major_complication_tid',$parameters['major_complications'],'IN');
    $query->Condition('patient_complication.entity_type','node');
    $query->Condition('patient_complication.deleted',0);
  }

  if(!empty($parameters['study_categories'])){
    $query->innerJoin('field_data_field_patient_study_category', 'patient_study_category' , 'patient_study_category.entity_id = node.nid');
    $query->Condition('patient_study_category.field_patient_study_category_tid',$parameters['study_categories'],'IN');
    $query->Condition('patient_study_category.entity_type','node');
    $query->Condition('patient_study_category.deleted',0);
  }
 // Condtionals End

  $query->orderBy('CAST(elapsed_time_bmt_value AS DECIMAL(10,2))', 'ASC');
  $result = $query->execute();
  //drupal_set_message("Query: ".$query->__toString());
  return $result;
}

function sc_set_probability($parameter,$survived_data, $time_elapsed, &$group_arr, &$survival_arr){
  // $patients_atrisk = $group_arr[$parameter]['REMAINING_PATIENTS']
  //   -($group_arr[$parameter]['CONTROL_PATIENTS']+$group_arr[$parameter]['DEAD_PATIENTS']);
  $patients_atrisk = $group_arr[$parameter]['CURR_MONTH_PATIENTS'];
  $group_arr[$parameter]['SURVIVAL_PROB'] = $group_arr[$parameter]['SURVIVAL_PROB']*($group_arr[$parameter]['REMAINING_PATIENTS']/$group_arr[$parameter]['CURR_MONTH_PATIENTS']); //Cumulative Survival
  //drupal_set_message("Death at: $diff_days for $parameter. Prob: ".$group_arr[$parameter]['SURVIVAL_PROB']);
  //drupal_set_message("For $parameter remaining: ".$group_arr[$parameter]['REMAINING_PATIENTS']);
  $group_arr[$parameter]['CURR_MONTH_PATIENTS'] = $group_arr[$parameter]['REMAINING_PATIENTS'];
  $survival_arr[$parameter][] = array('NUMMONTHS'=>$time_elapsed,'SURVIVAL_PROB'=>100*round($group_arr[$parameter]['SURVIVAL_PROB'],2));

}


function survival_curve_data($sc_parameters,$parameters){
  $result = sc_get_survdata($parameters);
  $min_survival_prob=1;
  $total_patients = $result->rowCount();
  $survival_arr['STATS'] = array('PATIENTS_TOTAL'=>$total_patients);
  //Generate Survival Curve Array for Each
  foreach ($sc_parameters as $key => $value) {
    $survival_arr[$value][] = array('NUMMONTHS'=>0,'SURVIVAL_PROB'=>100);
    $group_arr[$value] =  array('PATIENTS_TOTAL'=>$total_patients,'REMAINING_PATIENTS'=>$total_patients
      ,'CONTROL_PATIENTS'=>0,'DEAD_PATIENTS'=>0,'SURVIVAL_PROB'=>1,'NUM_MONTHS'=>0,'CURR_MONTH'=>0
      ,'CURR_MONTH_PATIENTS'=>$total_patients);
  }

  foreach ($result as $patient_rec) {
    //drupal_set_message("Nid is: ".$patient_rec->nid);
    $overall_survival = $patient_rec->overall_survival_value;
    $elapsed_month = $patient_rec->elapsed_time_bmt_value;
    $thal_free_survival = $patient_rec->thal_free_survival_value;
    //drupal_set_message($overall_survival." ".$elapsed_month." ".$thal_free_survival);
    if(is_numeric($overall_survival) && is_numeric($elapsed_month) && is_numeric($thal_free_survival)){
      $elapsed_month = round($elapsed_month,0);
        if($group_arr[SC_OVERALL]['CURR_MONTH'] != $elapsed_month){
          sc_set_probability(SC_OVERALL,$overall_survival, $elapsed_month, $group_arr, $survival_arr);
          $group_arr[SC_OVERALL]['CURR_MONTH'] = $elapsed_month;
        }
        if($overall_survival == 1){
        $group_arr[SC_OVERALL]['DEAD_PATIENTS']++;
        $group_arr[SC_OVERALL]['REMAINING_PATIENTS']--;
        }
        if($group_arr[SC_DISEASE_FREE]['CURR_MONTH']!= $elapsed_month){
          sc_set_probability(SC_DISEASE_FREE,$thal_free_survival,  $elapsed_month, $group_arr, $survival_arr);
          $group_arr[SC_DISEASE_FREE]['CURR_MONTH'] = $elapsed_month;
        }
        if($thal_free_survival == 1){
          $group_arr[SC_DISEASE_FREE]['DEAD_PATIENTS']++;
          $group_arr[SC_DISEASE_FREE]['REMAINING_PATIENTS']--;
        }
    }
  }
  return $survival_arr;
}

function survival_curve_render($parameter,$survival_arr=NULL){

  $options = new stdClass();

  // Diable credits.
  $options->credits->enabled = FALSE;

  // Chart.
  $options->chart = (object)array(
    'renderTo' => 'container',
    'type' => 'line',
  );

  $options->yAxis->title->text = "Survival Probability";
  $options->xAxis->title->text = "Timeline (In Months)" ;


  $options->yAxis->min=0;
  $options->yAxis->max=100;

  // Title.
  $options->title->text = t('Survival Chart');
  $options->title->x = -20;
  $options->subtitle->text = t('Total Patients: '.$survival_arr['STATS']['PATIENTS_TOTAL']);
  $options->subtitle->x = -20;



  // Tooltip.
  // Normally formatter is a function callback. For now we'll make it a string.
  // @todo whenever this is user defined (views config, etc) be sure to sanitize
  // this string before passing to highcharts_render().
  //$options->tooltip->formatter = "function() {return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';}";
  $options->tooltip->formatter = "function() {return '<b>'+ this.series.name +'</b>: Month: '+ this.x +', Probability: '+ this.y+'%';}";

  $options->legend= (object)array(
    'layout' => 'vertical',
    'align' => 'right',
    'verticalAlign' => 'top',
    'x' => -10,
    'y' => 100,
    'borderWidth' => 0
  );

  $options->series = array();
  foreach ($parameter as $key => $value) {
    $series = new StdClass();
    $series->type = 'line';
    $series->name = $value;
    $series->data = array();
    $series->step = TRUE;
    foreach ($survival_arr[$value] as $key => $surv_data) {
     $series->data[] = array($surv_data['NUMMONTHS'], $surv_data['SURVIVAL_PROB']);
     //$series->survivors = $surv_data['REMAINING_PATIENTS'];
    }

    $options->series[] = $series;
    $series = NULL;
  }

  if (is_object($options)) {
    // Optionally add styles or any other valid attributes, suitable for
    // drupal_attributes().
    $attributes = array('style' => array('height: 600px;'));
    return highcharts_render($options, $attributes);
  }
}




